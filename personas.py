import json
import pymongo
from flask import make_response, abort
from flask_api import status

def escribirJson(data):
    with open('personas.json', 'w') as outfile:
        json.dump(data, outfile, indent=4)

def cargarJson():    
    try:
        with open('personas.json') as json_file:
            data = json.load(json_file)
    except ValueError:                
        data = {}
    
    except FileNotFoundError:       
        data = {}
    
    return data

def adicionarPersona(persona):  
    cliente = pymongo.MongoClient("mongodb+srv://dbpython:azrael0526@cluster0-sf2xm.mongodb.net/test?retryWrites=true&w=majority")

    dataBase = cliente["apipersonas"]
    collection = dataBase["personas"]

    #pymongo.errors.DuplicateKeyError
    try:
        collection.insert_one(persona)
    except pymongo.errors.DuplicateKeyError:
        return {"resultado" : "El documento ya existe"}, status.HTTP_409_CONFLICT

    return {"resultado" : "Persona creada exitosamente"}, status.HTTP_200_OK

def actualizarPersona(persona):  
    cliente = pymongo.MongoClient("mongodb+srv://dbpython:azrael0526@cluster0-sf2xm.mongodb.net/test?retryWrites=true&w=majority")

    dataBase = cliente["apipersonas"]
    collection = dataBase["personas"]

    #pymongo.errors.DuplicateKeyError
    
    myquery = { "documento": persona["documento"]}
    newvalues = { "$set": persona}

    salida = collection.update_one(myquery, newvalues)

    print(salida.matched_count)

    if salida.matched_count == 1:
        return {"resultado" : "El documento fue modificado"}, status.HTTP_200_OK
    else:
        return {"resultado" : "El documento no existe"}, status.HTTP_409_CONFLICT


def adicionarHijo(documentoPadre, documento, hijo):
    data = cargarJson()

    if str(documentoPadre) in data:
        persona = data[str(documentoPadre)]

        if "hijos" not in persona:
            persona["hijos"] = []

        for hijito in persona["hijos"]:
            if str(documento) in hijito:
                return 501
        
        persona["hijos"].append({documento:hijo})

        escribirJson(data)

        return 200
    else:
        return 404    

def obtenerPersona(documento):
    data = cargarJson()
    
    if str(documento) in data:
        return data[str(documento)]
    else:
        return 404        

def eliminarPersona(documento):
    data = cargarJson()
    
    if str(documento) in data:
        del data[str(documento)]
        escribirJson(data)
        
        return 201
    else:
        return 404   